from cryptography.fernet import Fernet

from tor_weather.env import get_env

from .encode import encode_to_base64

EMAIL_TOKEN_EXPIRY = 30 * 60


def encrypt(payload: str) -> str:
    secret = get_env("EMAIL_ENCRYPT_PASS")
    encoded_secret: bytes = encode_to_base64(secret)
    cipher_suite: Fernet = Fernet(encoded_secret)
    return cipher_suite.encrypt(str.encode(payload)).decode("utf-8")


def decrypt(payload: str) -> str:
    secret = get_env("EMAIL_ENCRYPT_PASS")
    encoded_secret: bytes = encode_to_base64(secret)
    cipher_suite: Fernet = Fernet(encoded_secret)
    return cipher_suite.decrypt(str.encode(payload), ttl=EMAIL_TOKEN_EXPIRY).decode(
        "utf-8"
    )
