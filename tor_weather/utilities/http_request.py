from typing import Any

import requests

from tor_weather.extensions import job_logger


def http_request(endpoint: str) -> Any:
    try:
        response: Any = requests.get(endpoint).json()
        return response
    except Exception as e:
        job_logger.exception("Request to Onionoo Failed", e)
