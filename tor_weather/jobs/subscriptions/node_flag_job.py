from datetime import datetime
from typing import Any, Type

from tor_weather.core.mail import NodeFlagEmail
from tor_weather.extensions import job_logger
from tor_weather.jobs.classes.relay import RelayObject
from tor_weather.jobs.classes.subscription import SubscriptionObject
from tor_weather.jobs.subscriptions.subscription_job import SubscriptionJob
from tor_weather.utilities import time_diff


class NodeFlagSubscriptionJob(SubscriptionJob):
    def __init__(
        self, flag: str, subscription_obj: SubscriptionObject, relay_obj: RelayObject
    ) -> None:
        super().__init__(subscription_obj, relay_obj, flag)
        self.email_class: Type[NodeFlagEmail] = NodeFlagEmail  # type: ignore
        self.child_sub_name: str = f"node_flag_{flag}_sub"
        self.set_child_subscription(self.child_sub_name)

    def get_data_for_mail(self) -> Any:
        return {
            "fingerprint": self.relay.fingerprint,
            "wait_for": self.subscription.get_value("wait_for"),
            "flag": self.flag,
        }

    def validate(self) -> None:
        if self.subscription.child_sub and self.subscription.get_value("is_active"):
            if not self.subscription.get_value("issue_first_seen"):
                # Relay previously had the flag
                if not self.relay.flag[self.flag]:  # type: ignore
                    # Relay was running fine & has just lost the flag
                    self.subscription.set_value("issue_first_seen", datetime.utcnow())
            else:
                # Relay previously had the flag missing
                if self.relay.flag[self.flag]:  # type: ignore
                    # Relay was previously missing the flag and just got it back
                    self.subscription.set_value("issue_first_seen", None)
                    self.subscription.set_value("emailed", False)
                else:
                    # Relay previously had the flag missing & is still missing it
                    issue_first_seen = self.subscription.get_value("issue_first_seen")
                    wait_for = self.subscription.get_value("wait_for")
                    if time_diff(issue_first_seen, datetime.utcnow()) >= wait_for:
                        # Relay has been missing the flag for more than the waiting time
                        if not self.subscription.get_value("emailed"):
                            # Subscriber was not sent an email already
                            mail_data = self.get_data_for_mail()
                            try:
                                self.send_email(mail_data)
                                self.subscription.set_value("emailed", True)
                            except Exception as e:
                                job_logger.exception(f"Failed to send an email - {e}")
            self.subscription.commit()
