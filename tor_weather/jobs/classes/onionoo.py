from typing import List, cast

from tor_weather.env import get_env
from tor_weather.models.details_payload import (
    DetailsPayloadInterface,
    RelayPayloadInterface,
)
from tor_weather.utilities import http_request


class Onionoo:
    """Class representing the Onionoo Response"""

    @classmethod
    def _replace_reserved_keyword_keys(cls, data: dict) -> DetailsPayloadInterface:
        """Replaces the keys with reserved keynames with alternate keynames

        Args:
            data (dict): The initial API Payload

        Returns:
            DetailsPayloadInterface: API Payload with reserved keynames replaced
        """
        relay_data: dict = data["relays"]
        for relay in relay_data:
            relay["as_number"] = relay.pop("as")
        return cast(DetailsPayloadInterface, data)

    @classmethod
    def _get_relays(cls, data: DetailsPayloadInterface) -> List[RelayPayloadInterface]:
        """Gets the relays from the API Payload

        Args:
            data (dict): Massaged API Payload

        Returns:
            List[RelayPayloadInterface]: List of Relays
        """
        return data["relays"]

    @classmethod
    def get_relays(cls) -> List[RelayPayloadInterface]:
        """Fetches the list of relays from the Onionoo API"""
        endpoint = f"{get_env('API_URL')}/details"
        response: dict = http_request(endpoint)
        translated_response = cls._replace_reserved_keyword_keys(response)
        return cls._get_relays(translated_response)
