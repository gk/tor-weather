from typing import Any, TypedDict


class SubscriptionTypeInterface(TypedDict):
    internal_value: str
    db_value: str
    display_name: str
    category: Any
