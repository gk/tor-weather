import logging
from datetime import datetime

from flask_sqlalchemy import SQLAlchemy

db: SQLAlchemy = SQLAlchemy()

current_timestamp: str = datetime.now().strftime("%y-%m-%d-%H:%M:%S")


handler: logging.FileHandler = logging.FileHandler(
    f"logs/tor-weather-{current_timestamp}.log"
)
formatter: logging.Formatter = logging.Formatter(
    "%(asctime)s : %(name)s : %(levelname)s : %(message)s", "%m/%d/%Y %I:%M:%S %p"
)
handler.setFormatter(formatter)

backend_logger: logging.Logger = logging.getLogger("BACKEND")
backend_logger.setLevel(logging.DEBUG)
backend_logger.addHandler(handler)

job_logger: logging.Logger = logging.getLogger("JOB")
job_logger.setLevel(logging.DEBUG)
job_logger.addHandler(handler)
