from typing import cast

from flask_login import current_user, login_required
from flask_restx import Resource

from tor_weather.core.database.tables.subscriber import Subscriber
from tor_weather.error import BadRequest
from tor_weather.routes import api_ns
from tor_weather.service.subscription.node_flag import NodeFlagService

request_parser = api_ns().parser()
request_parser.add_argument(
    "fingerprint",
    type=str,
    help="Fingerprint of the Relay",
    required=True,
    location="form",
)
request_parser.add_argument(
    "wait_for",
    type=float,
    help="Time to wait before sending an email",
    required=True,
    location="form",
)


class NodeFlagSubscriptionCreateApi(Resource):
    """Implements the Node Flag Subscription Create API"""

    @login_required
    @api_ns().doc(parser=request_parser)
    @api_ns().response(302, "Redirect to the Subscription List Page")
    def post(self, flag_name: str):
        subscriber = cast(Subscriber, current_user)
        args = request_parser.parse_args()

        if args["fingerprint"] and args["wait_for"]:
            return NodeFlagService(subscriber.email, flag_name).create_subscription(
                fingerprint=args["fingerprint"],
                wait_for=int(float(args["wait_for"])),
            )
        else:
            raise BadRequest("Required inputs not passed")
