from typing import cast

from flask import Response
from flask_login import current_user, login_required
from flask_restx import Resource

from tor_weather.core.database.tables.subscriber import Subscriber
from tor_weather.routes import api_ns
from tor_weather.service.subscription.node_bandwidth import NodeBandwidthService


class NodeBandwidthSubscriptionActivateApi(Resource):
    """Implements the Node Bandwidth Subscription Activate API"""

    @login_required
    @api_ns().response(302, "Redirect to the Subscription List Page")
    def post(self, fingerprint: str) -> Response:
        subscriber = cast(Subscriber, current_user)
        return NodeBandwidthService(subscriber.email).activate_subscription(fingerprint)
