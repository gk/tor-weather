from typing import cast

from flask import Response
from flask_login import current_user, login_required
from flask_restx import Resource

from tor_weather.core.database.tables.subscriber import Subscriber
from tor_weather.routes import api_ns
from tor_weather.service.subscription.node_down import NodeDownService


class NodeDownSubscriptionActivateApi(Resource):
    """Implements the Node Down Subscription Activate API"""

    @login_required
    @api_ns().response(302, "Redirect to the Subscription List Page")
    def post(self, fingerprint: str) -> Response:
        subscriber = cast(Subscriber, current_user)
        return NodeDownService(subscriber.email).activate_subscription(fingerprint)
