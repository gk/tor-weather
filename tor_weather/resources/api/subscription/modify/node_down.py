from typing import cast

from flask import Response
from flask_login import current_user, login_required
from flask_restx import Resource

from tor_weather.core.database.tables.subscriber import Subscriber
from tor_weather.error import BadRequest
from tor_weather.routes import api_ns
from tor_weather.service.subscription.node_down import NodeDownService

request_parser = api_ns().parser()
request_parser.add_argument(
    "fingerprint",
    type=str,
    help="Fingerprint of the node to be modified",
    required=True,
    location="form",
)

request_parser.add_argument(
    "wait_for",
    type=float,
    help="Time to wait before sending an email",
    required=True,
    location="form",
)


class NodeDownSubscriptionModifyApi(Resource):
    """Implements the Node Down Subscription Modify API"""

    @login_required
    @api_ns().doc(parser=request_parser)
    @api_ns().response(302, "Redirect to the Subscription List Page")
    def post(self) -> Response:
        subscriber = cast(Subscriber, current_user)
        args = request_parser.parse_args()
        if args["fingerprint"] and args["wait_for"]:
            return NodeDownService(subscriber.email).modify_subscription(
                fingerprint=args["fingerprint"],
                wait_for=int(float(args["wait_for"])),
            )
        else:
            raise BadRequest("Required inputs not passed")
