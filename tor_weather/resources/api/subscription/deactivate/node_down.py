from typing import cast

from flask_login import current_user, login_required
from flask_restx import Resource

from tor_weather.core.database.tables.subscriber import Subscriber
from tor_weather.routes import api_ns
from tor_weather.service.subscription.node_down import NodeDownService


class NodeDownSubscriptionDeactivateApi(Resource):
    """Implements the Node Down Subscription Deactivate API"""

    @login_required
    @api_ns().response(302, "Redirect to the Subscription List Page")
    def post(self, fingerprint: str):
        subscriber = cast(Subscriber, current_user)
        return NodeDownService(email=subscriber.email).deactivate_subscription(
            fingerprint
        )
