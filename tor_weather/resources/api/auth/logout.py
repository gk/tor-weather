from flask import Response, make_response, redirect, url_for
from flask_login import logout_user
from flask_restx import Resource

from tor_weather.routes import api_ns


class LogoutApi(Resource):
    """Implements all API's related to Logout"""

    @api_ns().response(302, "Redirect to the Login Screen")
    def get(self) -> Response:
        logout_user()
        return make_response(redirect(url_for("login")))
