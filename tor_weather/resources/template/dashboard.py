from flask import make_response, redirect, url_for
from flask.wrappers import Response
from flask_login import login_required
from flask_restx import Resource

from tor_weather.extensions import backend_logger
from tor_weather.routes import template_ns


class DashboardHomeTemplate(Resource):
    """Implements the Dashboard's Home Page"""

    @login_required
    @template_ns().response(301, "Redirect to the current home page for dashboard")
    def get(self) -> Response:
        backend_logger.info("Dashboard Home Template Requested")
        # Currently, we don't have a home page. We redirect to the node-down subscription page
        return make_response(redirect(url_for("node_down_list")))
