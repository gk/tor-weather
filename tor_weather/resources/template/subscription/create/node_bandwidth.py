from flask import make_response, render_template, request
from flask.wrappers import Response
from flask_login import login_required
from flask_restx import Resource

from tor_weather.extensions import backend_logger
from tor_weather.routes import template_ns
from tor_weather.service.breadcrumb import Breadcrumb
from tor_weather.service.info_card import InformationCard
from tor_weather.service.sidebar import Sidebar
from tor_weather.service.subscription_form import SubscriptionForm


class NodeBandwidthCreateTemplate(Resource):
    """Implements the Node-Bandwidth Subscription Create Page"""

    subscription_category: str = "node-status"
    subscription_name: str = "node-bandwidth"
    form_fields: list[str] = ["fingerprint", "wait_for", "threshold"]

    @login_required
    @template_ns().response(200, "Success")
    def get(self) -> Response:
        backend_logger.info("Node Bandwidth Create Template Requested")
        sidebar = Sidebar(request.path).get_data()
        breadcrumb = Breadcrumb(request.path).get_data()
        informationCard = InformationCard().get_data(self.form_fields)
        subscriptionForm = SubscriptionForm(
            subscription_category=self.subscription_category,
            subscription_name=self.subscription_name,
        ).create_sub(self.form_fields)
        return make_response(
            render_template(
                "/pages/dashboard/subscription-edit-create.html",
                sidebar=sidebar,
                breadcrumb=breadcrumb,
                subscriptionForm=subscriptionForm,
                informationCard=informationCard,
            )
        )
