from typing import Type, cast

from flask import make_response, render_template, request
from flask.wrappers import Response
from flask_login import current_user, login_required
from flask_restx import Resource

from tor_weather.core.database.tables.subscriber import Subscriber
from tor_weather.extensions import backend_logger
from tor_weather.routes import template_ns
from tor_weather.service.breadcrumb import Breadcrumb
from tor_weather.service.info_card import InformationCard
from tor_weather.service.sidebar import Sidebar
from tor_weather.service.subscription.node_bandwidth import NodeBandwidthService
from tor_weather.service.subscription_form import SubscriptionForm


class NodeBandwidthModifyTemplate(Resource):
    """Implements the Node-Bandwidth Subscription Create Page"""

    subscription_category: str = "node-status"
    subscription_name: str = "node-bandwidth"
    subscription_service: Type[NodeBandwidthService] = NodeBandwidthService
    form_fields: list[str] = ["fingerprint", "wait_for", "threshold"]

    def _get_subscription_data(self, email: str, fingerprint: str):
        """Get data for subscription"""
        sub_data = self.subscription_service(email).get_subscription_data(fingerprint)
        return {
            "fingerprint": fingerprint,
            "wait_for": sub_data.wait_for,
            "threshold": int(sub_data.threshold),
        }

    @login_required
    @template_ns().response(200, "Success")
    def get(self, fingerprint: str) -> Response:
        backend_logger.info("Node Bandwidth Modify Template Requested")

        subscriber = cast(Subscriber, current_user)

        sidebar = Sidebar(request.path).get_data()
        breadcrumb = Breadcrumb(request.path).get_data()
        informationCard = InformationCard().get_data(self.form_fields)
        subscriptionForm = SubscriptionForm(
            subscription_category=self.subscription_category,
            subscription_name=self.subscription_name,
        ).modify_sub(
            self.form_fields, self._get_subscription_data(subscriber.email, fingerprint)
        )
        return make_response(
            render_template(
                "/pages/dashboard/subscription-edit-create.html",
                sidebar=sidebar,
                breadcrumb=breadcrumb,
                subscriptionForm=subscriptionForm,
                informationCard=informationCard,
            )
        )
