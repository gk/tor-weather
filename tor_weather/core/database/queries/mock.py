import os
from datetime import datetime

from tor_weather.core.database.tables import (
    NodeBandwidthSub,
    NodeDownSub,
    NodeFlagExitSub,
    NodeFlagFastSub,
    NodeFlagGuardSub,
    NodeFlagStableSub,
    NodeFlagValidSub,
    Relay,
    Subscriber,
    Subscription,
)
from tor_weather.extensions import db
from tor_weather.utilities.camel_case_recursive import camel_case_recursive
from tor_weather.utilities.read_json import read_json


def insert_subscriber_data() -> None:
    """Insert mock data for subscribers"""
    mock_file_path: str = "../../../../mock/database/subscribers.json"
    mock_data = read_json(os.path.join(os.path.dirname(__file__), mock_file_path))
    subscribers = camel_case_recursive(mock_data).get("payload")
    for subscriber in subscribers:
        subscriber_obj = Subscriber(
            id=subscriber.get("id"),
            email=subscriber.get("email"),
            password=subscriber.get("password"),
            is_confirmed=subscriber.get("isConfirmed"),
        )
        db.session.add(subscriber_obj)
    db.session.commit()


def insert_relay_data() -> None:
    """Insert mock data for relays"""
    mock_file_path: str = "../../../../mock/database/relays.json"
    mock_data = read_json(os.path.join(os.path.dirname(__file__), mock_file_path))
    relays = camel_case_recursive(mock_data).get("payload")
    for relay in relays:
        relay_obj = Relay(
            id=relay.get("id"),
            is_exit=relay.get("isExit"),
            fingerprint=relay.get("fingerprint"),
            first_seen=datetime.utcnow(),
            last_seen=datetime.utcnow(),
            version=relay.get("version"),
            recommended_version=relay.get("recommendedVersion"),
            is_up=relay.get("isUp"),
        )
        db.session.add(relay_obj)
    db.session.commit()


def insert_subscription_data() -> None:
    """Insert mock data for subscriptions"""
    mock_file_path: str = "../../../../mock/database/subscriptions.json"
    mock_data = read_json(os.path.join(os.path.dirname(__file__), mock_file_path))
    subscriptions = camel_case_recursive(mock_data).get("payload")
    for subscription in subscriptions:
        subscription_obj = Subscription(
            id=subscription.get("id"),
            subscriber_id=subscription.get("subscriberId"),
            relay_id=subscription.get("relayId"),
        )
        db.session.add(subscription_obj)
    db.session.commit()


def insert_node_down_sub_data() -> None:
    """Insert mock data for node down subscription"""
    mock_file_path: str = "../../../../mock/database/node_down_sub.json"
    mock_data = read_json(os.path.join(os.path.dirname(__file__), mock_file_path))
    subscriptions = camel_case_recursive(mock_data).get("payload")
    for subscription in subscriptions:
        subscription_obj = NodeDownSub(
            id=subscription.get("id"),
            is_active=subscription.get("isActive"),
            wait_for=subscription.get("waitFor"),
            subscription_id=subscription.get("subscriptionId"),
        )
        db.session.add(subscription_obj)
    db.session.commit()


def insert_node_bandwidth_sub_data() -> None:
    """Insert mock data for node bandwidth subscription"""
    mock_file_path: str = "../../../../mock/database/node_bandwidth_sub.json"
    mock_data = read_json(os.path.join(os.path.dirname(__file__), mock_file_path))
    subscriptions = camel_case_recursive(mock_data).get("payload")
    for subscription in subscriptions:
        subscription_obj = NodeBandwidthSub(
            id=subscription.get("id"),
            is_active=subscription.get("isActive"),
            threshold=subscription.get("threshold"),
            wait_for=subscription.get("waitFor"),
            subscription_id=subscription.get("subscriptionId"),
        )
        db.session.add(subscription_obj)
    db.session.commit()


def insert_node_flag_exit_sub_data() -> None:
    """Insert mock data for node flag exit subscription"""
    mock_file_path: str = "../../../../mock/database/node_flag_exit_sub.json"
    mock_data = read_json(os.path.join(os.path.dirname(__file__), mock_file_path))
    subscriptions = camel_case_recursive(mock_data).get("payload")
    for subscription in subscriptions:
        subscription_obj = NodeFlagExitSub(
            id=subscription.get("id"),
            is_active=subscription.get("isActive"),
            wait_for=subscription.get("waitFor"),
            subscription_id=subscription.get("subscriptionId"),
        )
        db.session.add(subscription_obj)
    db.session.commit()


def insert_node_flag_fast_sub_data() -> None:
    """Insert mock data for node flag fast subscription"""
    mock_file_path: str = "../../../../mock/database/node_flag_fast_sub.json"
    mock_data = read_json(os.path.join(os.path.dirname(__file__), mock_file_path))
    subscriptions = camel_case_recursive(mock_data).get("payload")
    for subscription in subscriptions:
        subscription_obj = NodeFlagFastSub(
            id=subscription.get("id"),
            is_active=subscription.get("isActive"),
            wait_for=subscription.get("waitFor"),
            subscription_id=subscription.get("subscriptionId"),
        )
        db.session.add(subscription_obj)
    db.session.commit()


def insert_node_flag_guard_sub_data() -> None:
    """Insert mock data for node flag guard subscription"""
    mock_file_path: str = "../../../../mock/database/node_flag_guard_sub.json"
    mock_data = read_json(os.path.join(os.path.dirname(__file__), mock_file_path))
    subscriptions = camel_case_recursive(mock_data).get("payload")
    for subscription in subscriptions:
        subscription_obj = NodeFlagGuardSub(
            id=subscription.get("id"),
            is_active=subscription.get("isActive"),
            wait_for=subscription.get("waitFor"),
            subscription_id=subscription.get("subscriptionId"),
        )
        db.session.add(subscription_obj)
    db.session.commit()


def insert_node_flag_stable_sub_data() -> None:
    """Insert mock data for node flag stable subscription"""
    mock_file_path: str = "../../../../mock/database/node_flag_stable_sub.json"
    mock_data = read_json(os.path.join(os.path.dirname(__file__), mock_file_path))
    subscriptions = camel_case_recursive(mock_data).get("payload")
    for subscription in subscriptions:
        subscription_obj = NodeFlagStableSub(
            id=subscription.get("id"),
            is_active=subscription.get("isActive"),
            wait_for=subscription.get("waitFor"),
            subscription_id=subscription.get("subscriptionId"),
        )
        db.session.add(subscription_obj)
    db.session.commit()


def insert_node_flag_valid_sub_data() -> None:
    """Insert mock data for node flag valid subscription"""
    mock_file_path: str = "../../../../mock/database/node_flag_valid_sub.json"
    mock_data = read_json(os.path.join(os.path.dirname(__file__), mock_file_path))
    subscriptions = camel_case_recursive(mock_data).get("payload")
    for subscription in subscriptions:
        subscription_obj = NodeFlagValidSub(
            id=subscription.get("id"),
            is_active=subscription.get("isActive"),
            wait_for=subscription.get("waitFor"),
            subscription_id=subscription.get("subscriptionId"),
        )
        db.session.add(subscription_obj)
    db.session.commit()


def drop_tables() -> None:
    """Drop all the tables"""
    print("(Initiating) - Deletion of Tables")
    meta = db.metadata
    for table in reversed(meta.sorted_tables):
        db.session.execute(table.delete())
    db.session.commit()
    print("(Completed) - Deletion of Tables")


def insert_mock_data() -> None:
    """Insert mock data for all the tables"""
    print("(Initiating) - Insertion of Mock Data")
    insert_subscriber_data()
    insert_relay_data()
    insert_subscription_data()
    # Inserting Subscriptions
    insert_node_down_sub_data()
    insert_node_bandwidth_sub_data()
    # Inserting Flag Subscriptions
    insert_node_flag_exit_sub_data()
    insert_node_flag_fast_sub_data()
    insert_node_flag_guard_sub_data()
    insert_node_flag_stable_sub_data()
    insert_node_flag_valid_sub_data()
    print("(Completed) - Insertion of Mock Data")


def initiate_tables() -> None:
    """Drop & Create all the tables with mock data"""
    drop_tables()
    insert_mock_data()
