from datetime import datetime

from tor_weather.extensions import db


class NodeVersionSub(db.Model):
    # The primary id for the node down subscription
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    # If the subscription is active (controlled by the subscriber from the dashboard)
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    # Creation date for the subscription
    created_on = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    # When was the subscription last updated by the user
    updated_on = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    eol_issue_first_seen = db.Column(db.DateTime, nullable=True)
    eol_subscription = db.Column(db.Boolean, nullable=False, default=False)
    eol_sub_emailed = db.Column(db.Boolean, nullable=False, default=False)
    outdated_subscription = db.Column(db.Boolean, nullable=False, default=False)
    outdated_sub_emailed = db.Column(db.Boolean, nullable=False, default=False)
    # Foreign key for mapping it with the subscriptions table
    subscription_id = db.Column(
        db.Integer, db.ForeignKey("subscription.id"), nullable=False
    )
    subscription = db.relationship("Subscription", back_populates="node_version_sub")
