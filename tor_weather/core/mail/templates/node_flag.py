from typing import Any

from .base_template import Email

subject = "Tor Weather :  Node-Flag [{flag}] Alert"

body = """

Hi,

We have detected that the node - {fingerprint} associated with your subscription has lost the {flag} flag
for more than {wait_for}hr(s). This may impact the performance of your service.

Thank You!

"""


class NodeFlagEmail(Email):
    def __init__(self, receiver: str, data: Any):
        super().__init__(receiver)
        self.email_subject = self._create_subject(data)
        self.email_body = self._create_body(data)

    def _create_body(self, data: Any) -> str:
        return body.format(
            fingerprint=data.get("fingerprint"),
            wait_for=data.get("wait_for"),
            flag=data.get("flag").title(),
        )

    def _create_subject(self, data: Any) -> str:
        return subject.format(
            flag=data.get("flag").title(),
        )
