from typing import Any

from .base_template import Email

subject = "Welcome to Tor-Weather!"

body = """

Hello,

We are excited to welcome you to the Tor-Weather service! Tor-Weather is a tool that allows you to monitor the health
and performance of your Tor node in real time, so you can quickly identify and address any issues that may arise.

To get started, simply log in to your account and create a new subscription!

Thank You!

"""


class WelcomeEmail(Email):
    def __init__(self, receiver: str, data: Any):
        super().__init__(receiver)
        self.email_subject: str = self._create_subject(data)
        self.email_body: str = self._create_body(data)

    # Bakes the content for the email
    def _create_body(self, data) -> str:
        return body.format()

    def _create_subject(self, data) -> str:
        return subject.format()
