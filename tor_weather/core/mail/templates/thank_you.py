from typing import Any

from .base_template import Email

subject = "Thank you for running a Tor node!"

body = """

Hello,

We want to thank you for running a Tor node and contributing to the Tor network. Your support is greatly appreciated
and helps to ensure that the network remains strong and secure.

In addition, we would like to let you know about a new service that we are offering called "Tor-Weather." Tor-Weather
allows you to monitor the health and performance of your node in real time, so you can quickly identify and address any
issues that may arise.

If you are interested in learning more about Tor-Weather, please visit our website - {website}.

Thank you again for your support.

"""


class ThankYouEmail(Email):
    def __init__(self, receiver: str, data: Any):
        super().__init__(receiver)
        self.email_subject = self._create_subject(data)
        self.email_body = self._create_body(data)

    def _create_body(self, data: Any) -> str:
        return body.format()

    def _create_subject(self, data: Any) -> str:
        return subject.format()
