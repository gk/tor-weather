from typing import Any

from .base_template import Email

subject = "Tor-Weather : Verify your email address"

body = """

Hi,

Welcome to the Tor-Weather Service.
Congratulations, you have successfully completed your registration. We just need to verify your
email to finish account registration.

{base_url}/api/verify?code={verification_code}

Thank You!

"""


class AccountVerificationEmail(Email):
    def __init__(self, receiver: str, data: Any):
        super().__init__(receiver)
        self.email_subject: str = self._create_subject(data)
        self.email_body: str = self._create_body(data)

    def _create_body(self, data: Any) -> str:
        return body.format(
            verification_code=data.get("verification_code"),
            base_url=data.get("base_url"),
        )

    def _create_subject(self, data: Any) -> str:
        return subject.format()
