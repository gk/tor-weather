import smtplib
from email.message import EmailMessage

from tor_weather.core.mail.templates.base_template import Email
from tor_weather.env import get_env
from tor_weather.error import InternalError
from tor_weather.extensions import backend_logger
from tor_weather.utilities import is_truthy


class Smtp:
    def __init__(self) -> None:
        self.email = get_env("SMTP_USERNAME")
        self.password = get_env("SMTP_PASSWORD")
        self.host = get_env("SMTP_HOST")
        self.port = int(get_env("SMTP_PORT"))
        self.ssl = is_truthy(get_env("SMTP_SSL"))
        self.smtp: smtplib.SMTP_SSL | smtplib.SMTP

    # Creates a SMTP Connection with SSL
    def _initiate_connection(self) -> None:
        """Initiates a connection with the SMTP Server"""
        if self.ssl:
            self.smtp = smtplib.SMTP_SSL(self.host, self.port)
        else:
            self.smtp = smtplib.SMTP(self.host, self.port)

        if self.password:
            self.smtp.login(self.email, self.password)

    # Generates a message for the email
    def _generate_message(self, mail_content: Email):
        """Generates the message to be sent to the user

        Args:
            mailContent (Email): Details for the email including receiver, subject & body

        Returns:
            _type_: Message to be sent via SMTP
        """
        message = EmailMessage()
        message["Subject"] = mail_content.email_subject
        message["From"] = self.email
        message["To"] = mail_content.receiver
        message.set_content(mail_content.email_body)
        return message

    # Sends a single email
    def send_mail(self, mail_content: Email, count=5) -> None:
        """Send a single email via SMTP

        Args:
            mail_content (Email): Details for the email including receiver, subject & body
            count (int, optional): Retry count left. Defaults to 5.
        """
        try:
            self._initiate_connection()

            fromaddr = self.email
            toaddr = mail_content.receiver

            email_text = (
                f"From: {fromaddr}\r\nTo: {toaddr}\r\nSubject: {mail_content.email_subject}"
                f"\r\n\r\n{mail_content.email_body}"
            )

            self.smtp.sendmail(fromaddr, [toaddr], email_text)
        except Exception as e:
            backend_logger.error("Issue connecting with SMTP Server")
            raise InternalError() from e
