from typing import Type

from flask import Response, url_for

from tor_weather.dao.subscription.node_bandwidth import NodeBandwidthDao
from tor_weather.service.subscription_wrapper import SubscriptionService


class NodeBandwidthService(SubscriptionService):

    TABLE_HEADER_KEYS: list[str] = ["fingerprint", "threshold", "wait_for", "is_active"]
    TABLE_DAO: Type[NodeBandwidthDao] = NodeBandwidthDao  # type: ignore

    def __init__(self, email: str) -> None:
        super().__init__(email)

    def _get_modify_url(self, id: int) -> str:
        return url_for("node_bandwidth_modify", fingerprint=id)

    def _get_delete_url(self, id: str) -> str:
        return url_for("node_bandwidth_delete_api", fingerprint=id)

    def _get_enable_url(self, id: str) -> str:
        return url_for("node_bandwidth_enable_api", fingerprint=id)

    def _get_disable_url(self, id: str) -> str:
        return url_for("node_bandwidth_disable_api", fingerprint=id)

    def get_subscriptions(self):
        """Get all the Node Bandwidth Subscriptions for the user"""
        return {
            "header": super()._get_table_header(),
            "content": super()._get_table_content(),
        }

    def get_subscription_data(self, fingerprint: str):
        """Get data for Node Bandwidth Subscription for the user"""
        return self.TABLE_DAO(email=self.email).get_subscription_data(fingerprint)

    def create_subscription(
        self, fingerprint: str, wait_for: int, threshold: int
    ) -> Response:
        """Create a Node Bandwidth Subscription"""
        data = {
            "fingerprint": fingerprint,
            "wait_for": wait_for,
            "threshold": int(threshold),
        }
        return self.TABLE_DAO(email=self.email).create_subscription(data)

    def modify_subscription(
        self, fingerprint: str, wait_for: int, threshold: int
    ) -> Response:
        """Modify a Node Bandwidth Subscription"""
        data = {
            "fingerprint": fingerprint,
            "wait_for": wait_for,
            "threshold": int(threshold),
        }
        return self.TABLE_DAO(email=self.email).modify_subscription(data)

    def deactivate_subscription(self, fingerprint: str) -> Response:
        """Deactivate a Node Bandwidth Subscription"""
        data = {"fingerprint": fingerprint}
        return self.TABLE_DAO(email=self.email).deactivate_subscription(data)

    def activate_subscription(self, fingerprint: str) -> Response:
        """Activate a Node Bandwidth Subscription"""
        data = {"fingerprint": fingerprint}
        return self.TABLE_DAO(email=self.email).activate_subscription(data)

    def delete_subscription(self, fingerprint: str) -> Response:
        """Delete a Node Bandwidth Subscription"""
        data = {"fingerprint": fingerprint}
        return self.TABLE_DAO(email=self.email).delete_subscription(data)
