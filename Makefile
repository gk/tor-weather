dev:
	poetry run python3 -m tor_weather.app
job:
	poetry run python3 -m tor_weather.job
datamodel:
	poetry run python3 -m tor_weather.data_model
prod: 
	gunicorn -w 1 -b localhost:5000 tor_weather.app:app --daemon
prod-debug:
	gunicorn -w 1 -b localhost:5000 tor_weather.app:app
mock-data:
	poetry run python mock/mock-server.py